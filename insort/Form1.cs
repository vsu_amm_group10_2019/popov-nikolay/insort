﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace insort
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int[] array;

        private void MakeTable()
        {
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = array.Length;
            dataGridView1.ClearSelection();
            for (int i = 0; i < array.Length; i++)
            {
                dataGridView1.Rows[0].Cells[i].Value = array[i];
            }
            dataGridView1.Refresh();
            Delay(1000);
        }

        private void SortSimple()
        {
            int range = 0;
            for (int i = 1; i < array.Length; i++)
            { 
                int r = range, x = array[i];
                dataGridView1.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.Gray;
                Delay(1000);
                while (r >= 0 && x < array[r])
                {
                    array[r + 1] = array[r];
                    r--;
                }
                array[r + 1] = x;
                range++;
                MakeTable();
                dataGridView1.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.White;
            }
        }

        private void SortBinary()
        {
            for (int i = 1; i < array.Length; i++)
            {
                dataGridView1.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.Gray;
                Delay(1000);
                int m, l = 0, r = i - 1;
                while (l < r)
                {
                    m = (l + r) / 2;
                    if (array[m] <= array[i])
                    {
                        l = m + 1;
                    }
                    else
                    {
                        r = m;
                    }
                }
                int x = array[i];
                if (array[i] < array[l])
                {
                    for (int j = i, k = 0; k < i - r; j--, k++)
                    {
                        array[j] = array[j - 1];
                    }
                    array[r] = x;
                }
                MakeTable();
                dataGridView1.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.White;
            }
        }

        private void Delay(int timeDelay)
        {
            int i = 0;
            var delayTimer = new System.Timers.Timer();
            delayTimer.Interval = timeDelay;
            delayTimer.AutoReset = false;
            delayTimer.Elapsed += (s, args) => i = 1;
            delayTimer.Start();
            while (i == 0) { };
        }

        private void ButtonClick()
        {
            int n = (int)numericUpDown1.Value;
            Random rnd = new Random();
            int min = (int)numericUpDown2.Value;
            int max = (int)numericUpDown3.Value;
            array = new int[n];
            for (int i = 0; i < n; i++)
            {
                array[i] = rnd.Next(min, max);
            }
            MakeTable();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ButtonClick();
            SortSimple();
            MessageBox.Show("Готово!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ButtonClick();
            SortBinary();
            MessageBox.Show("Готово!");
        }
    }
}
